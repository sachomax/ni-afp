module Main where

import Data.Maybe (fromMaybe)
import System.Environment (lookupEnv)

import DebtDiary

portVar = "DEBT_DIARY_PORT"
portDef = "3000"
dbnmVar = "DEBT_DIARY_DB_NAME"
dbnmDef = "sqlite.db"


main :: IO ()
main = do
    mPort <- lookupEnv portVar
    mDbNm <- lookupEnv dbnmVar
    let port = fromMaybe portDef mPort
    let path = fromMaybe dbnmDef mDbNm
    runApplication path (read port)
