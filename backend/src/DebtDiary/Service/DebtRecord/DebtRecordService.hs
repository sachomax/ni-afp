module DebtDiary.Service.DebtRecord.DebtRecordService where

import Control.Lens ((^.))
import Control.Monad.Reader (liftIO)
import Data.UUID (toString)
import Data.UUID.V4 (nextRandom)
import Data.Maybe

import DebtDiary.Api.Resources.DebtRecord.DebtRecordDTO as DTO
import DebtDiary.Api.Resources.DebtRecord.DebtRecordCreateDTO as CDTO
import DebtDiary.Api.Resources.DebtRecord.DebtRecordUpdateDTO as UDTO
import DebtDiary.Context
import DebtDiary.Database.DAO.DebtRecordDAO
import qualified DebtDiary.Database.DAO.PersonDAO as Person (getById, getByKey, getKeyById)
import DebtDiary.Database.Model
import DebtDiary.Service.DebtRecord.DebtRecordMapper
    ( fromCreateDTO, fromUpdateDTO, toDTO )

getAllRecords :: Maybe String -> AppContextM [DebtRecordDTO]
getAllRecords mPersonId = do
  records <- case mPersonId of
              Just personId -> do
                mPersonKey <- Person.getKeyById personId
                case mPersonKey of
                  Just personKey -> do
                    getAllByPersonId personKey
                  _ -> return []
              _ -> do
                getAll Nothing Nothing
  people1 <- sequenceA $ heGetPersonDebtor . Just <$> records
  people2 <- sequenceA $ heGetPersonCreditor . Just <$> records
  return $ zipWith3 toDTO records (fromJust <$> people1) (fromJust <$> people2)

getRecord :: String -> AppContextM (Maybe DebtRecordDTO)
getRecord recordId = do
  mRecord <- getById recordId
  mPersonDebtor <- heGetPersonDebtor mRecord
  mPersonCreditor <- heGetPersonCreditor mRecord
  return $ toDTO <$> mRecord <*> mPersonDebtor <*> mPersonCreditor

createRecord :: DebtRecordCreateDTO -> AppContextM (Maybe DebtRecordDTO)
createRecord createDto = do
  newUuid <- liftIO $ toString <$> nextRandom
  mPersonKeyDebtor <- Person.getKeyById (createDto ^. CDTO.personUuidDebtor)
  mPersonKeyCreditor <- Person.getKeyById (createDto ^. CDTO.personUuidCreditor)
  case mPersonKeyDebtor of
    Nothing       -> return Nothing
    Just personIdDebtor -> do
      case mPersonKeyCreditor of
        Nothing -> return Nothing
        Just personIdCreditor -> do

          let newRecord = fromCreateDTO newUuid createDto personIdDebtor personIdCreditor
          _ <- create newRecord
          getRecord newUuid

updateRecord :: String -> DebtRecordCreateDTO -> AppContextM (Maybe DebtRecordDTO)
updateRecord recordId createDto = do
  mPersonKeyDebtor <- Person.getKeyById (createDto ^. CDTO.personUuidDebtor)
  mPersonKeyCreditor <- Person.getKeyById (createDto ^. CDTO.personUuidCreditor)
  case mPersonKeyDebtor of
    Nothing       -> return Nothing
    Just personIdDebtor -> do
      case mPersonKeyCreditor of
        Nothing -> return Nothing
        Just personIdCreditor -> do
          let updatedRecord = fromCreateDTO recordId createDto personIdDebtor personIdCreditor
          mRecord <- update recordId updatedRecord
          mPersonDebtor <- Person.getByKey personIdDebtor
          mPersonCreditor <- Person.getByKey personIdCreditor
          return $ toDTO <$> mRecord <*> mPersonDebtor <*> mPersonCreditor

patchRecord :: String -> DebtRecordUpdateDTO -> AppContextM (Maybe DebtRecordDTO)
patchRecord recordId updateDto = do
  mRecord <- getById recordId
  case mRecord of
    (Just record) -> do
      mPersonDebtor <- maybe (heGetPersonDebtor mRecord) Person.getById (updateDto ^. UDTO.personUuidDebtor)
      mPersonCreditor <- maybe (heGetPersonCreditor mRecord) Person.getById (updateDto ^. UDTO.personUuidCreditor)
      case mPersonDebtor of
        (Just personDebtor) -> do
          mPersonDebtorId <- Person.getKeyById $ DebtDiary.Database.Model.personUuid personDebtor
          case mPersonCreditor of
            (Just personCreditor) -> do
              mPersonCreditorId <- Person.getKeyById $ DebtDiary.Database.Model.personUuid personCreditor
              case mPersonDebtorId of
                (Just personIdDebtor) -> do
                  case mPersonCreditorId of
                    (Just personIdCreditor) -> do
                      let updatedRecord = fromUpdateDTO record updateDto personIdDebtor personIdCreditor
                      mNewRecord <- update recordId updatedRecord
                      return $ toDTO <$> mNewRecord <*> mPersonDebtor <*> mPersonCreditor
                    _ -> return Nothing
                _ -> return Nothing
            _ -> return Nothing
        _ -> return Nothing
    _ -> return Nothing

deleteRecord :: String -> AppContextM Bool
deleteRecord recordId = deleteById recordId

-- Helpers
heGetPersonDebtor :: Maybe DebtRecord -> AppContextM (Maybe Person)
heGetPersonDebtor Nothing = return Nothing
heGetPersonDebtor (Just record) = Person.getByKey $ debtRecordPersonIdDebtor record

heGetPersonCreditor :: Maybe DebtRecord -> AppContextM (Maybe Person)
heGetPersonCreditor Nothing = return Nothing
heGetPersonCreditor (Just record) = Person.getByKey $ debtRecordPersonIdCreditor record
