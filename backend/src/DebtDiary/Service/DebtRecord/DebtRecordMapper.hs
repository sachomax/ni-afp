module DebtDiary.Service.DebtRecord.DebtRecordMapper where

import Control.Lens ((^.))
import Data.Maybe (fromMaybe)

import qualified DebtDiary.Api.Resources.DebtRecord.DebtRecordDTO as DTO
import qualified DebtDiary.Api.Resources.DebtRecord.DebtRecordCreateDTO as CDTO
import qualified DebtDiary.Api.Resources.DebtRecord.DebtRecordUpdateDTO as UDTO
import DebtDiary.Database.Model
import DebtDiary.Database.Model (DebtRecord(debtRecordPersonIdCreditor), Person (personUuid))
import DebtDiary.Api.Resources.DebtRecord.DebtRecordUpdateDTO (DebtRecordUpdateDTO(_debtRecordUpdateDTOPersonUuidDebtor))

fromCreateDTO :: String -> CDTO.DebtRecordCreateDTO -> PersonId -> PersonId -> DebtRecord
fromCreateDTO newUuid createDto personIdDebtor personIdCreditor =
  DebtRecord 
    newUuid
    (createDto ^. CDTO.amount)
    (createDto ^. CDTO.timestamp)
    (createDto ^. CDTO.note)
    personIdDebtor
    personIdCreditor

fromUpdateDTO :: DebtRecord -> UDTO.DebtRecordUpdateDTO -> PersonId -> PersonId ->  DebtRecord
fromUpdateDTO DebtRecord{..} updateDto personIdDebtor personIdCreditor =
  DebtRecord 
    debtRecordUuid
    (fromMaybe debtRecordAmount    (updateDto ^. UDTO.amount))
    (fromMaybe debtRecordTimestamp (updateDto ^. UDTO.timestamp))
    (fromMaybe debtRecordNote      (updateDto ^. UDTO.note))
    personIdDebtor
    personIdCreditor

toDTO :: DebtRecord -> Person -> Person -> DTO.DebtRecordDTO
toDTO DebtRecord{..} debtor creditor = DTO.DebtRecordDTO
  { _debtRecordDTOUuid       = debtRecordUuid
  , _debtRecordDTOAmount     = debtRecordAmount
  , _debtRecordDTOTimestamp  = debtRecordTimestamp
  , _debtRecordDTONote       = debtRecordNote
  , _debtRecordDTOPersonUuidDebtor = personUuid debtor
  , _debtRecordDTOPersonUuidCreditor = personUuid creditor
  }
