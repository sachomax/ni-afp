module DebtDiary.Service.Person.PersonMapper where

import Control.Lens ((^.))
import Data.Maybe (fromMaybe)

import DebtDiary.Api.Resources.Person.PersonDTO as DTO
import DebtDiary.Api.Resources.Person.PersonCreateDTO as CDTO
import DebtDiary.Api.Resources.Person.PersonSimpleDTO as SDTO
import DebtDiary.Api.Resources.Person.PersonUpdateDTO as UDTO
import DebtDiary.Database.Model

fromCreateDTO :: String -> CDTO.PersonCreateDTO -> Person
fromCreateDTO newUuid createDto =
  Person 
    newUuid
    (createDto ^. CDTO.firstName)
    (createDto ^. CDTO.lastName)
    (createDto ^. CDTO.payment)
    (createDto ^. CDTO.note)


fromUpdateDTO :: Person -> UDTO.PersonUpdateDTO -> Person
fromUpdateDTO Person{..} updateDto =
  Person 
    personUuid
    (fromMaybe personFirstName (updateDto ^. UDTO.firstName))
    (fromMaybe personLastName  (updateDto ^. UDTO.lastName))
    (fromMaybe personPayment   (updateDto ^. UDTO.payment))
    (fromMaybe personNote      (updateDto ^. UDTO.note))
    

toDTO :: Person -> [DebtRecord] -> DTO.PersonDTO
toDTO Person{..} personDebtRecords = DTO.PersonDTO
  { _personDTOUuid        = personUuid
  , _personDTOFirstName   = personFirstName
  , _personDTOLastName    = personLastName
  , _personDTONote        = personNote
  , _personDTOPayment     = personPayment
  , _personDTOCurrentDebt = sum $ debtRecordAmount <$> personDebtRecords
  , _personDTODebtRecords = toInteger . length $ personDebtRecords
  }

toSimpleDTO :: Person -> SDTO.PersonSimpleDTO
toSimpleDTO Person{..} = SDTO.PersonSimpleDTO
  { _personSimpleDTOUuid      = personUuid
  , _personSimpleDTOFirstName = personFirstName
  , _personSimpleDTOLastName  = personLastName
  , _personSimpleDTONote      = personNote
  , _personSimpleDTOPayment   = personPayment
  }
