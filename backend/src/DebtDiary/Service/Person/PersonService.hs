module DebtDiary.Service.Person.PersonService where

import Control.Monad.Reader (liftIO)
import Data.UUID (toString)
import Data.UUID.V4 (nextRandom)

import DebtDiary.Api.Resources.Person.PersonDTO
import DebtDiary.Api.Resources.Person.PersonCreateDTO
    ( PersonCreateDTO )
import DebtDiary.Api.Resources.Person.PersonSimpleDTO
import DebtDiary.Api.Resources.Person.PersonUpdateDTO
import DebtDiary.Context
import DebtDiary.Database.DAO.DebtRecordDAO as DebtRecord
import DebtDiary.Database.DAO.PersonDAO as Person
import DebtDiary.Service.Person.PersonMapper
    ( fromCreateDTO, fromUpdateDTO, toDTO, toSimpleDTO )

getAllPeople :: Maybe String -> AppContextM [PersonSimpleDTO]
getAllPeople query = do
  people <- Person.getAll query
  return $ toSimpleDTO <$> people

getPerson :: String -> AppContextM (Maybe PersonDTO)
getPerson personId = do
  mPerson <- Person.getById personId
  mPersonId <- Person.getKeyById personId
  personDebtRecords <- maybe (return mempty) DebtRecord.getAllByPersonId mPersonId
  return $ toDTO <$> mPerson <*> return personDebtRecords

createPerson :: PersonCreateDTO -> AppContextM (Maybe PersonSimpleDTO)
createPerson createDto = do
  newUuid <- liftIO $ toString <$> nextRandom
  let newPerson = fromCreateDTO newUuid createDto
  _ <- Person.create newPerson
  mPerson <- Person.getById newUuid
  return $ toSimpleDTO <$> mPerson

updatePerson :: String -> PersonCreateDTO -> AppContextM (Maybe PersonSimpleDTO)
updatePerson personId createDto = do
  let updatedPerson = fromCreateDTO personId createDto
  mPerson <- Person.update personId updatedPerson
  return $ toSimpleDTO <$> mPerson

patchPerson :: String -> PersonUpdateDTO -> AppContextM (Maybe PersonSimpleDTO)
patchPerson personId updateDto = do
  mPerson <- Person.getById personId
  case mPerson of
    (Just person) -> do
      let updatedPerson = fromUpdateDTO person updateDto
      mNewPerson <- Person.update personId updatedPerson
      return $ toSimpleDTO <$> mNewPerson
    _ -> return Nothing

deletePerson :: String -> AppContextM Bool
deletePerson personId = Person.deleteById personId
