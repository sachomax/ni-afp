module DebtDiary.Service.DebtSummary.DebtSummaryService where

import Data.Function
import Data.List
import Data.Maybe
import Data.Time

import DebtDiary.Service.DebtRecord.DebtRecordService
import DebtDiary.Api.Resources.DebtSummary.DebtSummaryBriefDTO as DebtSummaryBriefDTO (DebtSummaryBriefDTO)
import DebtDiary.Api.Resources.DebtSummary.DebtSummaryDTO as DebtSummaryMapper (DebtSummaryDTO)
import DebtDiary.Context
import qualified DebtDiary.Database.DAO.DebtRecordDAO as DebtRecord
import DebtDiary.Database.Model
import qualified DebtDiary.Service.DebtSummary.DebtSummaryMapper as DebtSummaryMapper
import qualified DebtDiary.Service.DebtSummary.DebtSummaryBriefMapper as DebtSummaryBriefMapper
import qualified DebtDiary.Database.DAO.PersonDAO as Person
import Control.Monad.Except
import Servant
import DebtDiary.Api.Resources.DebtSummary.DebtSummaryTotalDTO (DebtSummaryTotalDTO)
import qualified DebtDiary.Service.DebtSummary.DebtSummaryTotalMapper as DebtSummaryTotalMapper

getBriefSummary :: Maybe UTCTime -> Maybe UTCTime -> String -> AppContextM DebtSummaryBriefDTO
getBriefSummary s u personString = do
  mPersonId <- Person.getKeyById personString 
  case mPersonId of
    Just personId -> do
      records <- DebtRecord.getAllByPersonId personId
      return $ DebtSummaryBriefMapper.toDTO s u (groupBy ((==) `on` debtRecordPersonIdCreditor) records) personString personId
    _ -> throwError err404

getSummary :: Maybe UTCTime -> Maybe UTCTime -> String -> AppContextM DebtSummaryDTO
getSummary s u personString = do
  mPersonId <- Person.getKeyById personString 
  case mPersonId of
    Just personId -> do
      records <- DebtRecord.getAll s u
      let groupedRecords = groupBy ((==) `on` debtRecordPersonIdDebtor) records
      personPerRecordGroup1 <- sequenceA $ heGetPersonDebtor . Just . head <$> groupedRecords
      personPerRecordGroup2 <- sequenceA $ heGetPersonCreditor . Just . head <$> groupedRecords
      let personPerRecordGroup = (fromJust <$> personPerRecordGroup1) ++ (fromJust <$> personPerRecordGroup2)
      return $ DebtSummaryMapper.toDTO s u groupedRecords personPerRecordGroup personString personId
    _ -> throwError err404


getTotalSummary :: String -> String -> AppContextM DebtSummaryTotalDTO
getTotalSummary creditor debtor = do
  mPersonIdCreditor <- Person.getKeyById creditor 
  case mPersonIdCreditor of
    Just personIdCreditor -> do
      mPersonIdDebtor <- Person.getKeyById debtor
      case mPersonIdDebtor of
        Just personIdDebtor -> do
          creditorRecords <- DebtRecord.getAllByCreditorIdAndDebtorId personIdCreditor personIdDebtor
          debtorRecords <- DebtRecord.getAllByDebtorIdAndCreditorId personIdDebtor personIdCreditor
          return $ DebtSummaryTotalMapper.toDTO creditorRecords debtorRecords creditor
        _ -> throwError err404 
    _ -> throwError err404
