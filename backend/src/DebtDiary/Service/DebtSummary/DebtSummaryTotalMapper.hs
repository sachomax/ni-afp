module DebtDiary.Service.DebtSummary.DebtSummaryTotalMapper where

import DebtDiary.Api.Resources.DebtSummary.DebtSummaryTotalDTO as DTO
import DebtDiary.Database.Model

toDTO :: [DebtRecord] -> [DebtRecord] -> String -> DTO.DebtSummaryTotalDTO
toDTO creditor debtor personString = DTO.DebtSummaryTotalDTO
  { _debtSummaryTotalDTOTotal   = sum (debtRecordAmount <$> creditor ) - sum (debtRecordAmount <$> debtor )
  , _debtSummaryTotalDTOPerson  = personString
  }