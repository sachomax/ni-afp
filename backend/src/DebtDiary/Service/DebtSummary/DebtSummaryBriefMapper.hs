module DebtDiary.Service.DebtSummary.DebtSummaryBriefMapper where

import Data.Time

import DebtDiary.Api.Resources.DebtSummary.DebtSummaryBriefDTO as DTO
import DebtDiary.Database.Model

toDTO :: Maybe UTCTime -> Maybe UTCTime -> [[DebtRecord]] -> String -> PersonId -> DTO.DebtSummaryBriefDTO
toDTO s u groupedRecords personString personId = DTO.DebtSummaryBriefDTO
  { _debtSummaryBriefDTOTotal   = sum (debtRecordAmount <$> filter (\record -> debtRecordPersonIdCreditor record == personId) (concat groupedRecords)) - sum (debtRecordAmount <$> filter (\record -> debtRecordPersonIdDebtor record == personId) (concat groupedRecords))
  , _debtSummaryBriefDTORecords = toInteger . length . concat $ groupedRecords
  , _debtSummaryBriefDTOSince   = s
  , _debtSummaryBriefDTOUntil   = u
  , _debtSummaryBriefDTOPeople  = toInteger . length $ groupedRecords
  , _debtSummaryBriefDTOPerson  = personString
  }
