module DebtDiary.Service.DebtSummary.DebtSummaryMapper where

import Data.Time

import DebtDiary.Api.Resources.DebtSummary.DebtSummaryDTO as DTO
import DebtDiary.Database.Model
import qualified DebtDiary.Service.Person.PersonMapper as PersonMapper

toDTO :: Maybe UTCTime -> Maybe UTCTime -> [[DebtRecord]] -> [Person] -> String -> PersonId -> DTO.DebtSummaryDTO
toDTO s u groupedRecords personPerRecordGroup personString personId = DTO.DebtSummaryDTO
  { _debtSummaryDTOTotal   = sum (debtRecordAmount <$> filter (\record -> debtRecordPersonIdCreditor record == personId) (concat groupedRecords)) - sum (debtRecordAmount <$> filter (\record -> debtRecordPersonIdDebtor record == personId) (concat groupedRecords))
  , _debtSummaryDTORecords = toInteger . length . concat $ groupedRecords
  , _debtSummaryDTOSince   = s
  , _debtSummaryDTOUntil   = u
  , _debtSummaryDTOPeople  = zipWith PersonMapper.toDTO personPerRecordGroup groupedRecords
  , _debtSummaryDTOPerson  = personString
  }