module DebtDiary.Database.Model where

import Control.Lens
import Control.Monad.Reader (ask, liftIO)
import Data.Time
import Database.Persist.Sqlite
import Database.Persist.TH

import DebtDiary.Context
import qualified Control.Monad.Trans.Reader
import qualified Data.Pool
import qualified Control.Monad.Reader.Class
import qualified Control.Monad.IO.Class

-- See: https://www.yesodweb.com/book/persistent
-- IDs are handled by Persistent

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Person
  uuid       String
  firstName  String
  lastName   String
  payment    String
  note       String
  deriving Eq Read Show

DebtRecord
  uuid              String
  amount            Double
  timestamp         UTCTime default=CURRENT_TIME
  note              String
  personIdDebtor    PersonId OnDeleteCascade
  personIdCreditor  PersonId OnDeleteCascade
  deriving Eq Read Show
|]

runDB :: (Control.Monad.Reader.Class.MonadReader s m, Control.Monad.IO.Class.MonadIO m, BackendCompatible SqlBackend backend, HasPool s (Data.Pool.Pool backend)) => Control.Monad.Trans.Reader.ReaderT backend IO b -> m b
runDB query = do
  context <- ask
  liftIO $ runSqlPool query (context ^. pool)
