module DebtDiary.Database.DAO.DebtRecordDAO where

import Database.Persist
import Data.Maybe
import Data.Time

import DebtDiary.Database.Model
import DebtDiary.Context

getKeyById :: String -> AppContextM (Maybe DebtRecordId)
getKeyById recordId = do
  result <- runDB $ selectList [DebtRecordUuid ==. recordId] []
  case result of
    []    -> return Nothing
    ((Entity key _):_) -> return $ Just key

getByKey :: DebtRecordId -> AppContextM (Maybe DebtRecord)
getByKey key = runDB $ get key

getById :: String -> AppContextM (Maybe DebtRecord)
getById recordId = do
  result <- runDB $ selectList [DebtRecordUuid ==. recordId] []
  case result of
    []    -> return Nothing
    ((Entity _ record):_) -> return $ Just record

getAll :: Maybe UTCTime -> Maybe UTCTime -> AppContextM [DebtRecord]
getAll s u = do
  result <- runDB $ selectList ([DebtRecordTimestamp >=. fromJust s | isJust s] ++ [DebtRecordTimestamp <=. fromJust u | isJust u]) []
  return $ extractRecord <$> result
  where
    extractRecord (Entity _ record) = record

getAllByPersonId :: PersonId -> AppContextM [DebtRecord]
getAllByPersonId personId = do
  result1 <- runDB $ selectList [DebtRecordPersonIdDebtor ==. personId] []
  result2 <- runDB $ selectList [DebtRecordPersonIdCreditor ==. personId] []
  let result = result1 ++ result2
  return $ extractRecord <$> result
  where
    extractRecord (Entity _ record) = record


getAllByCreditorIdAndDebtorId :: PersonId -> PersonId -> AppContextM [DebtRecord]
getAllByCreditorIdAndDebtorId creditor debtor = do
  result1 <- runDB $ selectList [DebtRecordPersonIdDebtor ==. debtor, DebtRecordPersonIdCreditor ==. creditor] []
  let result = result1
  return $ extractRecord <$> result
  where
    extractRecord (Entity _ record) = record


getAllByDebtorIdAndCreditorId :: PersonId -> PersonId -> AppContextM [DebtRecord]
getAllByDebtorIdAndCreditorId = getAllByCreditorIdAndDebtorId

create :: DebtRecord -> AppContextM DebtRecordId
create newRecord = runDB $ insert newRecord

update :: String -> DebtRecord -> AppContextM (Maybe DebtRecord)
update recordId replacement = do
  entityKey <- getKeyById recordId
  case entityKey of
    (Just key) -> do
      runDB $ replace key replacement
      getByKey key
    _ -> return Nothing

deleteById :: String -> AppContextM Bool
deleteById recordId = do
  entityKey <- getKeyById recordId
  case entityKey of
    (Just key) -> do
      runDB $ delete key
      return True
    _ -> return False
