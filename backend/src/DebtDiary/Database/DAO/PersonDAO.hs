module DebtDiary.Database.DAO.PersonDAO where

import Data.Maybe
import Database.Persist

import DebtDiary.Database.Model
import DebtDiary.Context


getKeyById :: String -> AppContextM (Maybe PersonId)
getKeyById personId = do 
  result <- runDB $ selectList [PersonUuid ==. personId] []
  case result of
    []    -> return $ Nothing
    ((Entity key _):_) -> return $ Just key

getByKey :: PersonId -> AppContextM (Maybe Person)
getByKey key = runDB $ get key

getByKeyList :: [PersonId] -> AppContextM [Person]
getByKeyList keys = do
  peopleEntities <- runDB $ selectList [PersonId <-. keys] []
  return $ extractPerson <$> peopleEntities
  where
    extractPerson (Entity _ record) = record

getById :: String -> AppContextM (Maybe Person)
getById personId = do
  result <- runDB $ selectList [PersonUuid ==. personId] []
  case result of
    []    -> return $ Nothing
    ((Entity _ person):_) -> return $ Just person

filterLike :: EntityField record String -> String -> Filter record
filterLike field str = Filter field (FilterValue ("%" ++ str ++ "%")) (BackendSpecificFilter "LIKE")

getAll :: Maybe String -> AppContextM [Person]
getAll query = do
  let conditions = [FilterOr [filterLike PersonFirstName (fromJust query), filterLike PersonLastName (fromJust query)] | isJust query]
  result <- runDB $ selectList conditions []
  return $ extractPerson <$> result
  where
    extractPerson (Entity _ person) = person

create :: Person -> AppContextM PersonId
create newPerson = runDB $ insert newPerson

update :: String -> Person -> AppContextM (Maybe Person)
update personId replacement = do
  entityKey <- getKeyById personId
  case entityKey of
    (Just key) -> do
      runDB $ replace key replacement
      getByKey key
    _ -> return Nothing

deleteById :: String -> AppContextM Bool
deleteById personId = do
  entityKey <- getKeyById personId
  case entityKey of
    (Just key) -> do
      runDB $ delete key
      return True
    _ -> return False
