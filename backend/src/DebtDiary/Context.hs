module DebtDiary.Context where

import Control.Lens (makeFields)
import Control.Monad.Except (ExceptT, MonadError)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Logger (LoggingT, MonadLogger)
import Control.Monad.Reader (MonadReader, ReaderT)
import Database.Persist.Sqlite (ConnectionPool)
import Servant (ServerError)

data AppContext =
  AppContext
    { _appContextPool :: ConnectionPool
    }

makeFields ''AppContext

-- | Monad for AppContext using MonadTransformers
--   ReaderT (to read the context)
--   LoggingT and ExceptT
newtype AppContextM a =
  AppContextM
    { runAppContextM :: ReaderT AppContext (LoggingT (ExceptT ServerError IO)) a
    }
  deriving (Applicative, Functor, Monad, MonadIO, MonadReader AppContext, MonadError ServerError, MonadLogger)
