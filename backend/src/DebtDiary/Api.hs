module DebtDiary.Api where

import Servant

import DebtDiary.Api.DebtRecordApi (DebtRecordAPI, debtRecordServer)
import DebtDiary.Api.DebtSummaryApi (DebtSummaryAPI, debtSummaryServer)
import DebtDiary.Api.PersonApi (PersonAPI, personServer)
import DebtDiary.Context

type AppAPI = DebtRecordAPI :<|> DebtSummaryAPI :<|> PersonAPI

appAPI :: Proxy AppAPI
appAPI = Proxy

appServer :: ServerT AppAPI AppContextM
appServer = debtRecordServer :<|> debtSummaryServer :<|> personServer
