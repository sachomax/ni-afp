module DebtDiary.Api.DebtSummaryApi where

import Servant
import Data.Maybe
import Data.Time

import DebtDiary.Api.Resources.DebtSummary.DebtSummaryBriefDTO
import DebtDiary.Api.Resources.DebtSummary.DebtSummaryDTO
import DebtDiary.Context
import DebtDiary.Service.DebtSummary.DebtSummaryService
import DebtDiary.Api.Resources.DebtSummary.DebtSummaryTotalDTO (DebtSummaryTotalDTO)


-- GET /summary?creditor={creditor}&debtor={debtor}
type Summary_GET = "summary" :> QueryParam "creditor" String :> QueryParam "debtor" String :> Get '[JSON] DebtSummaryTotalDTO

summary_GET :: Maybe String -> Maybe String -> AppContextM DebtSummaryTotalDTO
summary_GET creditor debtor
    | not (isJust creditor) || not (isJust debtor) = throwError err400
    | otherwise = getTotalSummary (fromJust creditor) (fromJust debtor)

-- GET /?since={since}&until={until}&personId={personId}
type List_GET = QueryParam "since" UTCTime :> QueryParam "until" UTCTime :> QueryParam "personId" String :> Get '[JSON] DebtSummaryDTO

list_GET :: Maybe UTCTime -> Maybe UTCTime -> Maybe String -> AppContextM DebtSummaryDTO
list_GET s u p
    | isJust p && isJust s && isJust u && s > u = throwError err400
    | otherwise = do
        case p of 
            Just personId -> do 
                getSummary s u personId
            _ -> throwError err400

-- GET /brief?since={since}&until={until}&personId={personId}
type Brief_GET = "brief" :> QueryParam "since" UTCTime :> QueryParam "until" UTCTime :> QueryParam "personId" String :> Get '[JSON] DebtSummaryBriefDTO

brief_GET :: Maybe UTCTime -> Maybe UTCTime -> Maybe String -> AppContextM DebtSummaryBriefDTO
brief_GET s u p
    | isJust p && isJust s && isJust u && s > u = throwError err400
    | otherwise = do
        case p of 
            Just personId -> do 
                getBriefSummary s u personId
            _ -> throwError err400

-- Debt Summary API composition
type DebtSummaryAPI = List_GET :<|> Brief_GET :<|> Summary_GET

debtSummaryAPI :: Proxy DebtSummaryAPI
debtSummaryAPI = Proxy

debtSummaryServer :: ServerT DebtSummaryAPI AppContextM
debtSummaryServer = list_GET :<|> brief_GET :<|> summary_GET
