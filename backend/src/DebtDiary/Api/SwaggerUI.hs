module DebtDiary.Api.SwaggerUI where

import Control.Lens
import Data.Swagger
import Servant
import Servant.Swagger
import Servant.Swagger.UI

import DebtDiary.Api
import DebtDiary.Api.Resources.DebtRecord.DebtRecordCreateDTO()
import DebtDiary.Api.Resources.DebtRecord.DebtRecordDTO()
import DebtDiary.Api.Resources.DebtRecord.DebtRecordUpdateDTO()
import DebtDiary.Api.Resources.Person.PersonCreateDTO()
import DebtDiary.Api.Resources.Person.PersonDTO()
import DebtDiary.Api.Resources.Person.PersonSimpleDTO()
import DebtDiary.Api.Resources.Person.PersonUpdateDTO()


type SwaggerAPI = SwaggerSchemaUI "swagger-ui" "swagger.json"

swagger :: Swagger
swagger = toSwagger appAPI & info . title .~ "Debt Diary API" & info . description ?~ "API specification for a simple debt diary"

swaggerServer :: Server SwaggerAPI
swaggerServer = swaggerSchemaUIServer swagger

