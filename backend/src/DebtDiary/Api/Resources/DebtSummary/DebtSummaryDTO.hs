module DebtDiary.Api.Resources.DebtSummary.DebtSummaryDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Time

import DebtDiary.Api.Resources.Person.PersonDTO

data DebtSummaryDTO = DebtSummaryDTO
  { _debtSummaryDTOTotal   :: Double
  , _debtSummaryDTORecords :: Integer
  , _debtSummaryDTOSince   :: Maybe UTCTime
  , _debtSummaryDTOUntil   :: Maybe UTCTime
  , _debtSummaryDTOPeople  :: [PersonDTO]
  , _debtSummaryDTOPerson  :: String
  } deriving (Show, Read, Eq)

makeFields ''DebtSummaryDTO

instance ToJSON DebtSummaryDTO where
  toJSON dto = object [
    "total"   .= (dto ^. total),
    "records" .= (dto ^. records),
    "since"   .= (dto ^. since),
    "until"   .= (dto ^. DebtDiary.Api.Resources.DebtSummary.DebtSummaryDTO.until),
    "people"  .= (dto ^. people),
    "person"  .= (dto ^. person)]

instance ToSchema DebtSummaryDTO where
  declareNamedSchema _ = do
    integerSchema <- declareSchemaRef (Proxy :: Proxy Integer)
    doubleSchema <- declareSchemaRef (Proxy :: Proxy Double)
    utcTimeSchema <- declareSchemaRef (Proxy :: Proxy UTCTime)
    personDTOListSchema <- declareSchemaRef (Proxy :: Proxy [PersonDTO])
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    return $ NamedSchema (Just "DebtSummaryDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("total", doubleSchema)
          , ("records", integerSchema)
          , ("since", utcTimeSchema)
          , ("until", utcTimeSchema)
          , ("people", personDTOListSchema)
          , ("person", stringSchema)
          ]
      & required .~ [ "total", "records", "people", "person" ]
