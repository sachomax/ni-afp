module DebtDiary.Api.Resources.DebtSummary.DebtSummaryTotalDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger


data DebtSummaryTotalDTO = DebtSummaryTotalDTO
  { _debtSummaryTotalDTOTotal   :: Double
  , _debtSummaryTotalDTOPerson  :: String
  } deriving (Show, Read, Eq)

makeFields ''DebtSummaryTotalDTO

instance ToJSON DebtSummaryTotalDTO where
  toJSON dto = object [
    "total"   .= (dto ^. total),
    "person"  .= (dto ^. person)]

instance ToSchema DebtSummaryTotalDTO where
  declareNamedSchema _ = do
    doubleSchema <- declareSchemaRef (Proxy :: Proxy Double)
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    return $ NamedSchema (Just "DebtSummaryTotalDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("total", doubleSchema)
          , ("person", stringSchema)
          ]
      & required .~ [ "total", "person" ]