module DebtDiary.Api.Resources.DebtSummary.DebtSummaryBriefDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Time

data DebtSummaryBriefDTO = DebtSummaryBriefDTO
  { _debtSummaryBriefDTOTotal   :: Double
  , _debtSummaryBriefDTORecords :: Integer
  , _debtSummaryBriefDTOSince   :: Maybe UTCTime
  , _debtSummaryBriefDTOUntil   :: Maybe UTCTime
  , _debtSummaryBriefDTOPeople  :: Integer
  , _debtSummaryBriefDTOPerson  :: String 
  } deriving (Show, Read, Eq)

makeFields ''DebtSummaryBriefDTO

instance FromJSON DebtSummaryBriefDTO where
  parseJSON = withObject "DebtSummaryDTO" $ \o -> do
    _debtSummaryBriefDTOTotal   <- o .: "total"
    _debtSummaryBriefDTORecords <- o .: "records"
    _debtSummaryBriefDTOSince   <- o .: "since"
    _debtSummaryBriefDTOUntil   <- o .: "until"
    _debtSummaryBriefDTOPeople  <- o .: "people"
    _debtSummaryBriefDTOPerson  <- o .: "person"
    return DebtSummaryBriefDTO{..}

instance ToJSON DebtSummaryBriefDTO where
  toJSON dto = object [
    "total"   .= (dto ^. total),
    "records" .= (dto ^. records),
    "since"   .= (dto ^. since),
    "until"   .= (dto ^. DebtDiary.Api.Resources.DebtSummary.DebtSummaryBriefDTO.until),
    "people"  .= (dto ^. people),
    "person"  .= (dto ^. person)]

instance ToSchema DebtSummaryBriefDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    intSchema <- declareSchemaRef (Proxy :: Proxy Int)
    utcTimeSchema <- declareSchemaRef (Proxy :: Proxy UTCTime)
    return $ NamedSchema (Just "DebtSummaryBriefDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("total", stringSchema)
          , ("records", intSchema)
          , ("since", utcTimeSchema)
          , ("until", stringSchema)
          , ("people", intSchema)
          , ("person", stringSchema)
          ]
      & required .~ [ "total", "records", "people", "person" ]
