module DebtDiary.Api.Resources.Person.PersonDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger

data PersonDTO = PersonDTO
  { _personDTOUuid        :: String
  , _personDTOFirstName   :: String
  , _personDTOLastName    :: String
  , _personDTONote        :: String
  , _personDTOPayment     :: String
  , _personDTOCurrentDebt :: Double
  , _personDTODebtRecords :: Integer
  } deriving (Show, Read, Eq)

makeFields ''PersonDTO

instance FromJSON PersonDTO where
  parseJSON = withObject "PersonDTO" $ \o -> do
    _personDTOUuid        <- o .: "id"
    _personDTOFirstName   <- o .: "firstName"
    _personDTOLastName    <- o .: "lastName"
    _personDTONote        <- o .: "note"
    _personDTOPayment     <- o .: "payment"
    _personDTOCurrentDebt <- o .: "currentDebt"
    _personDTODebtRecords <- o .: "debtRecords"
    return PersonDTO{..}

instance ToJSON PersonDTO where
  toJSON dto = object [
    "id"          .= (dto ^. uuid),
    "firstName"   .= (dto ^. firstName),
    "lastName"    .= (dto ^. lastName),
    "note"        .= (dto ^. note),
    "payment"     .= (dto ^. payment),
    "currentDebt" .= (dto ^. currentDebt),
    "debtRecords" .= (dto ^. debtRecords)]

instance ToSchema PersonDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    doubleSchema <- declareSchemaRef (Proxy :: Proxy Double)
    integerSchema <- declareSchemaRef (Proxy :: Proxy Integer)
    return $ NamedSchema (Just "PersonDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("id", stringSchema)
          , ("firstName", stringSchema)
          , ("lastName", stringSchema)
          , ("note", stringSchema)
          , ("payment", stringSchema)
          , ("currentDebt", doubleSchema)
          , ("debtRecords", integerSchema)
          ]
      & required .~ [ "id", "firstName", "lastName", "note", "payment", "currentDebt", "debtRecords" ]
