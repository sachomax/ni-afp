module DebtDiary.Api.Resources.Person.PersonCreateDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger

data PersonCreateDTO = PersonCreateDTO
  { _personCreateDTOFirstName :: String
  , _personCreateDTOLastName  :: String
  , _personCreateDTONote      :: String
  , _personCreateDTOPayment   :: String
  } deriving (Show, Read, Eq)

makeFields ''PersonCreateDTO

instance FromJSON PersonCreateDTO where
  parseJSON = withObject "PersonCreateDTO" $ \o -> do
    _personCreateDTOFirstName <- o .: "firstName"
    _personCreateDTOLastName  <- o .: "lastName"
    _personCreateDTONote      <- o .: "note"
    _personCreateDTOPayment   <- o .: "payment"
    return PersonCreateDTO{..}

instance ToJSON PersonCreateDTO where
  toJSON dto = object [
    "firstName" .= (dto ^. firstName),
    "lastName"  .= (dto ^. lastName),
    "note"      .= (dto ^. note),
    "payment"   .= (dto ^. payment)]

instance ToSchema PersonCreateDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    return $ NamedSchema (Just "PersonCreateDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("firstName", stringSchema)
          , ("lastName", stringSchema)
          , ("note", stringSchema)
          , ("payment", stringSchema)
          ]
      & required .~ [ "firstName", "lastName", "note", "payment" ]
