module DebtDiary.Api.Resources.Person.PersonUpdateDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger

data PersonUpdateDTO = PersonUpdateDTO
  { _personUpdateDTOFirstName :: Maybe String
  , _personUpdateDTOLastName  :: Maybe String
  , _personUpdateDTONote      :: Maybe String
  , _personUpdateDTOPayment   :: Maybe String
  } deriving (Show, Read, Eq)

makeFields ''PersonUpdateDTO

instance FromJSON PersonUpdateDTO where
  parseJSON = withObject "PersonUpdateDTO" $ \o -> do
    _personUpdateDTOFirstName <- o .:? "firstName"
    _personUpdateDTOLastName  <- o .:? "lastName"
    _personUpdateDTONote      <- o .:? "note"
    _personUpdateDTOPayment   <- o .:? "payment"

    return PersonUpdateDTO{..}

instance ToJSON PersonUpdateDTO where
  toJSON dto = object [
    "firstName" .= Just (dto ^. firstName),
    "lastName"  .= Just (dto ^. lastName),
    "note"      .= Just (dto ^. note),
    "payment"   .= Just (dto ^. payment)]

instance ToSchema PersonUpdateDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    return $ NamedSchema (Just "PersonUpdateDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("firstName", stringSchema)
          , ("lastName", stringSchema)
          , ("note", stringSchema)
          , ("payment", stringSchema)
          ]
      & required .~ []
