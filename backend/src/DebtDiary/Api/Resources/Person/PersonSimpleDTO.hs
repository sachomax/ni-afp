module DebtDiary.Api.Resources.Person.PersonSimpleDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger

data PersonSimpleDTO = PersonSimpleDTO
  { _personSimpleDTOUuid      :: String
  , _personSimpleDTOFirstName :: String
  , _personSimpleDTOLastName  :: String
  , _personSimpleDTONote      :: String
  , _personSimpleDTOPayment   :: String
  } deriving (Show, Read, Eq)

makeFields ''PersonSimpleDTO

instance FromJSON PersonSimpleDTO where
  parseJSON = withObject "PersonSimpleDTO" $ \o -> do
    _personSimpleDTOUuid      <- o .: "id"
    _personSimpleDTOFirstName <- o .: "firstName"
    _personSimpleDTOLastName  <- o .: "lastName"
    _personSimpleDTONote      <- o .: "note"
    _personSimpleDTOPayment   <- o .: "payment"
    return PersonSimpleDTO{..}

instance ToJSON PersonSimpleDTO where
  toJSON dto = object [
    "id"        .= (dto ^. uuid),
    "firstName" .= (dto ^. firstName),
    "lastName"  .= (dto ^. lastName),
    "note"      .= (dto ^. note),
    "payment"   .= (dto ^. payment)]

instance ToSchema PersonSimpleDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    return $ NamedSchema (Just "PersonSimpleDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("id", stringSchema)
          , ("firstName", stringSchema)
          , ("lastName", stringSchema)
          , ("note", stringSchema)
          , ("payment", stringSchema)
          ]
      & required .~ [ "id", "firstName", "lastName", "note", "payment" ]
