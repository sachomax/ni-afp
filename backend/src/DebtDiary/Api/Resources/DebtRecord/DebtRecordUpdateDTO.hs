module DebtDiary.Api.Resources.DebtRecord.DebtRecordUpdateDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Time

data DebtRecordUpdateDTO = DebtRecordUpdateDTO
  { _debtRecordUpdateDTOAmount              :: Maybe Double
  , _debtRecordUpdateDTOTimestamp           :: Maybe UTCTime
  , _debtRecordUpdateDTONote                :: Maybe String
  , _debtRecordUpdateDTOPersonUuidDebtor    :: Maybe String
  , _debtRecordUpdateDTOPersonUuidCreditor  :: Maybe String
  } deriving (Show, Read, Eq)

makeFields ''DebtRecordUpdateDTO

instance FromJSON DebtRecordUpdateDTO where
  parseJSON = withObject "DebtRecordUpdateDTO" $ \o -> do
    _debtRecordUpdateDTOAmount              <- o .:? "amount"
    _debtRecordUpdateDTOTimestamp           <- o .:? "timestamp"
    _debtRecordUpdateDTONote                <- o .:? "note"
    _debtRecordUpdateDTOPersonUuidDebtor    <- o .:? "personIdDebtor"
    _debtRecordUpdateDTOPersonUuidCreditor  <- o .:? "personIdCreditor"
    return DebtRecordUpdateDTO{..}

instance ToJSON DebtRecordUpdateDTO where
  toJSON dto = object [
    "amount"    .= (dto ^. amount),
    "timestamp" .= (dto ^. timestamp),
    "note"      .= (dto ^. note),
    "personIdDebtor"  .= (dto ^. personUuidDebtor),
    "personIdCreditor"  .= (dto ^. personUuidCreditor)]

instance ToSchema DebtRecordUpdateDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    doubleSchema <- declareSchemaRef (Proxy :: Proxy Double)
    utcTimeSchema <- declareSchemaRef (Proxy :: Proxy UTCTime)
    return $ NamedSchema (Just "DebtRecordUpdateDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("amount", doubleSchema)
          , ("timestamp", utcTimeSchema)
          , ("note", stringSchema)
          , ("personIdDebtor", stringSchema)
          , ("personIdCreditor", stringSchema)
          ]
      & required .~ [ ]
