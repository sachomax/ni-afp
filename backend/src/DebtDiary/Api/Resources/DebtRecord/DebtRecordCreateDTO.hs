module DebtDiary.Api.Resources.DebtRecord.DebtRecordCreateDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Time

data DebtRecordCreateDTO = DebtRecordCreateDTO
  { _debtRecordCreateDTOAmount              :: Double
  , _debtRecordCreateDTOTimestamp           :: UTCTime
  , _debtRecordCreateDTONote                :: String
  , _debtRecordCreateDTOPersonUuidDebtor    :: String
  , _debtRecordCreateDTOPersonUuidCreditor  :: String
  } deriving (Show, Read, Eq)

makeFields ''DebtRecordCreateDTO

instance FromJSON DebtRecordCreateDTO where
  parseJSON = withObject "DebtRecordCreateDTO" $ \o -> do
    _debtRecordCreateDTOAmount              <- o .: "amount"
    _debtRecordCreateDTOTimestamp           <- o .: "timestamp"
    _debtRecordCreateDTONote                <- o .: "note"
    _debtRecordCreateDTOPersonUuidDebtor    <- o .: "personIdDebtor"
    _debtRecordCreateDTOPersonUuidCreditor  <- o .: "personIdCreditor"
    return DebtRecordCreateDTO{..}

instance ToJSON DebtRecordCreateDTO where
  toJSON dto = object [
    "amount"    .= (dto ^. amount),
    "timestamp" .= (dto ^. timestamp),
    "note"      .= (dto ^. note),
    "personIdDebtor"  .= (dto ^. personUuidDebtor),
    "personIdCreditor"  .= (dto ^. personUuidCreditor)]

instance ToSchema DebtRecordCreateDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    doubleSchema <- declareSchemaRef (Proxy :: Proxy Double)
    utcTimeSchema <- declareSchemaRef (Proxy :: Proxy UTCTime)
    return $ NamedSchema (Just "DebtRecordCreateDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("amount", doubleSchema)
          , ("timestamp", utcTimeSchema)
          , ("note", stringSchema)
          , ("personIdDebtor", stringSchema)
          , ("personIdCreditor", stringSchema)
          ]
      & required .~ [ "amount", "timestamp", "note", "personIdDebtor", "personIdCreditor" ]
