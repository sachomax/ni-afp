module DebtDiary.Api.Resources.DebtRecord.DebtRecordDTO where

import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Proxy
import Data.Swagger
import Data.Time

data DebtRecordDTO = DebtRecordDTO
  { _debtRecordDTOUuid               :: String
  , _debtRecordDTOAmount             :: Double
  , _debtRecordDTOTimestamp          :: UTCTime
  , _debtRecordDTONote               :: String
  , _debtRecordDTOPersonUuidDebtor   :: String
  , _debtRecordDTOPersonUuidCreditor :: String
  } deriving (Show, Read, Eq)

makeFields ''DebtRecordDTO

instance FromJSON DebtRecordDTO where
  parseJSON = withObject "DebtRecordDTO" $ \o -> do
    _debtRecordDTOUuid       <- o .: "id"
    _debtRecordDTOAmount     <- o .: "amount"
    _debtRecordDTOTimestamp  <- o .: "timestamp"
    _debtRecordDTONote       <- o .: "note"
    _debtRecordDTOPersonUuidDebtor <- o .: "personIdDebtor"
    _debtRecordDTOPersonUuidCreditor <- o .: "personIdCreditor"
    return DebtRecordDTO{..}

instance ToJSON DebtRecordDTO where
  toJSON dto = object [
    "id"                .= (dto ^. uuid),
    "amount"            .= (dto ^. amount),
    "timestamp"         .= (dto ^. timestamp),
    "note"              .= (dto ^. note),
    "personIdDebtor"    .= (dto ^. personUuidDebtor),
    "personIdCreditor"  .= (dto ^. personUuidCreditor)]


instance ToSchema DebtRecordDTO where
  declareNamedSchema _ = do
    stringSchema <- declareSchemaRef (Proxy :: Proxy String)
    doubleSchema <- declareSchemaRef (Proxy :: Proxy Double)
    utcTimeSchema <- declareSchemaRef (Proxy :: Proxy UTCTime)
    return $ NamedSchema (Just "DebtRecordDTO") $ mempty
      & type_ ?~ SwaggerObject
      & properties .~
          [ ("id", stringSchema)
          , ("amount", doubleSchema)
          , ("timestamp", utcTimeSchema)
          , ("note", stringSchema)
          , ("personIdDebtor", stringSchema)
          , ("personIdCreditor", stringSchema)
          ]
      & required .~ [ "id", "amount", "timestamp", "note", "personIdDebtor", "personIdCreditor" ]
