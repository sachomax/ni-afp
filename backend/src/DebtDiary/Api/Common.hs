module DebtDiary.Api.Common where

import Data.Aeson
import Servant

import DebtDiary.Context

-- Note: this is a bit simplifying, better would be to keep error
-- in context and here decide if 404, 400, 500 or other should be 
-- thrown as response status
returnOr404 :: ToJSON a => Maybe a -> AppContextM a
returnOr404 Nothing  = throwError err404
returnOr404 (Just x) = return x
