module DebtDiary.Api.DebtRecordApi where

import Servant

import DebtDiary.Api.Common
import DebtDiary.Api.Resources.DebtRecord.DebtRecordDTO
import DebtDiary.Api.Resources.DebtRecord.DebtRecordCreateDTO
import DebtDiary.Api.Resources.DebtRecord.DebtRecordUpdateDTO
import DebtDiary.Context
import DebtDiary.Service.DebtRecord.DebtRecordService

-- GET /records?personId={personId}
type List_GET  = "records" :> QueryParam "personId" String :> Get '[JSON] [DebtRecordDTO]

list_GET :: Maybe String -> AppContextM [DebtRecordDTO]
list_GET = getAllRecords

-- POST /records [DebtRecordCreateDTO]
type List_POST = ReqBody '[JSON] DebtRecordCreateDTO :> "records" :> Verb 'POST 201 '[JSON] DebtRecordDTO

list_POST :: DebtRecordCreateDTO -> AppContextM DebtRecordDTO
list_POST reqDto = do
  mRecordDTO <- createRecord reqDto
  returnOr404 mRecordDTO

-- GET /records/{recordId}
type Detail_GET  = "records" :> Capture "recordId" String :> Get '[JSON] DebtRecordDTO

detail_GET :: String -> AppContextM DebtRecordDTO
detail_GET recordId = do
  mRecordDTO <- getRecord recordId
  returnOr404 mRecordDTO

-- PUT /records/{recordId} [DebtRecordCreateDTO]
type Detail_PUT  = ReqBody '[JSON] DebtRecordCreateDTO :> "records" :> Capture "recordId" String :> Verb 'PUT 201 '[JSON] DebtRecordDTO

detail_PUT :: DebtRecordCreateDTO -> String -> AppContextM DebtRecordDTO
detail_PUT reqDto recordId = do
  mRecordDTO <- updateRecord recordId reqDto
  returnOr404 mRecordDTO

-- PATCH /records/{recordId} [DebtRecordUpdateDTO]
type Detail_PATCH  = ReqBody '[JSON] DebtRecordUpdateDTO :> "records" :> Capture "recordId" String :> Verb 'PATCH 201 '[JSON] DebtRecordDTO

detail_PATCH :: DebtRecordUpdateDTO -> String -> AppContextM DebtRecordDTO
detail_PATCH reqDto recordId = do
  mRecordDTO <- patchRecord recordId reqDto
  returnOr404 mRecordDTO

-- DELETE /records/{recordId}
type Detail_DELETE  = "records" :> Capture "recordId" String :> Verb 'DELETE 204 '[JSON] NoContent

detail_DELETE :: String -> AppContextM NoContent
detail_DELETE recordId = do
  result <- deleteRecord recordId
  case result of
    True -> return NoContent
    _ -> throwError $ err404

-- Debt Record API composition
type DebtRecordAPI = List_GET :<|> List_POST :<|> Detail_GET :<|> Detail_PUT :<|> Detail_PATCH :<|> Detail_DELETE

debtRecordAPI :: Proxy DebtRecordAPI
debtRecordAPI = Proxy

debtRecordServer :: ServerT DebtRecordAPI AppContextM
debtRecordServer = list_GET :<|> list_POST :<|> detail_GET :<|> detail_PUT :<|> detail_PATCH :<|> detail_DELETE
