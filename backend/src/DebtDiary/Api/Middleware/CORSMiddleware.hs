module DebtDiary.Api.Middleware.CORSMiddleware
  ( corsMiddleware
  ) where

import Network.Wai (Middleware)
import Network.Wai.Middleware.Cors 

corsMiddleware :: Middleware
corsMiddleware = cors (const $ Just appCorsResourcePolicy)

appCorsResourcePolicy :: CorsResourcePolicy
appCorsResourcePolicy = CorsResourcePolicy {
    corsOrigins        = Nothing
  , corsMethods        = ["OPTIONS", "GET", "PUT", "POST", "PATCH", "DELETE", "HEAD"]
  , corsRequestHeaders = ["Authorization", "Content-Type", "Accept", "Authorization", "Origin"]
  , corsExposedHeaders = Nothing
  , corsMaxAge         = Nothing
  , corsVaryOrigin     = False
  , corsRequireOrigin  = False
  , corsIgnoreFailures = False
}
