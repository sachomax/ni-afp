module DebtDiary.Api.PersonApi where

import Servant

import DebtDiary.Api.Common
import DebtDiary.Api.Resources.Person.PersonDTO
import DebtDiary.Api.Resources.Person.PersonCreateDTO
import DebtDiary.Api.Resources.Person.PersonSimpleDTO
import DebtDiary.Api.Resources.Person.PersonUpdateDTO
import DebtDiary.Context
import DebtDiary.Service.Person.PersonService

-- GET /people?q={query}
type List_GET  = "people" :> QueryParam "q" String :> Get '[JSON] [PersonSimpleDTO]

list_GET :: Maybe String -> AppContextM [PersonSimpleDTO]
list_GET = getAllPeople

-- POST /people [PersonCreateDTO]
type List_POST = ReqBody '[JSON] PersonCreateDTO :> "people" :> Verb 'POST 201 '[JSON] PersonSimpleDTO

list_POST :: PersonCreateDTO -> AppContextM PersonSimpleDTO
list_POST reqDto = do
  mPersonDTO <- createPerson reqDto
  returnOr404 mPersonDTO

-- GET /people/{personId}
type Detail_GET = "people" :> Capture "personId" String :> Get '[JSON] PersonDTO

detail_GET :: String -> AppContextM PersonDTO
detail_GET personId = do
  mPersonDTO <- getPerson personId
  returnOr404 mPersonDTO

-- PUT /people/{personId} [PersonCreateDTO]
type Detail_PUT = ReqBody '[JSON] PersonCreateDTO :> "people" :> Capture "personId" String :> Verb 'PUT 200 '[JSON] PersonSimpleDTO

detail_PUT :: PersonCreateDTO -> String -> AppContextM PersonSimpleDTO
detail_PUT reqDto personId = do
  mPersonDTO <- updatePerson personId reqDto
  returnOr404 mPersonDTO

-- PATCH /people/{personId} [PersonUpdateDTO]
type Detail_PATCH = ReqBody '[JSON] PersonUpdateDTO :> "people" :> Capture "personId" String :> Verb 'PATCH 200 '[JSON] PersonSimpleDTO

detail_PATCH :: PersonUpdateDTO -> String -> AppContextM PersonSimpleDTO
detail_PATCH reqDto personId = do
  mPersonDTO <- patchPerson personId reqDto
  returnOr404 mPersonDTO

-- DELETE /people/{personId}
type Detail_DELETE = "people" :> Capture "personId" String :> Verb 'DELETE 204 '[JSON] NoContent

detail_DELETE :: String -> AppContextM NoContent
detail_DELETE personId = do
  result <- deletePerson personId
  case result of
    True -> return NoContent
    _ -> throwError $ err404

-- Person API composition
type PersonAPI = List_GET :<|> List_POST :<|> Detail_GET :<|> Detail_PUT :<|> Detail_PATCH :<|> Detail_DELETE

personAPI :: Proxy PersonAPI
personAPI = Proxy

personServer :: ServerT PersonAPI AppContextM
personServer = list_GET :<|> list_POST :<|> detail_GET :<|> detail_PUT :<|> detail_PATCH :<|> detail_DELETE
