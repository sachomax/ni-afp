module DebtDiary (runApplication) where

import Control.Monad.Logger (runStdoutLoggingT)
import Control.Monad.Reader (runReaderT)
import Data.Text as T
import Database.Persist.Sqlite (createSqlitePool, runSqlPool, runMigration)
import Network.Wai.Handler.Warp (run)
import Servant

import DebtDiary.Api (appAPI, AppAPI, appServer)
import DebtDiary.Api.Middleware.CORSMiddleware (corsMiddleware)
import DebtDiary.Api.SwaggerUI (swaggerServer, SwaggerAPI)
import DebtDiary.Context
import DebtDiary.Database.Model (migrateAll)

type ServerAPI = SwaggerAPI :<|> AppAPI

serverAPI :: Proxy ServerAPI
serverAPI = Proxy

-- | Convert application context to its monadic version for use in Handler
convertContext :: AppContext -> AppContextM a -> Handler a
convertContext ctx f = Handler $ runStdoutLoggingT $ runReaderT (runAppContextM f) ctx

-- | Build server for serving AppAPI
server :: AppContext -> Server ServerAPI
server ctx = swaggerServer :<|> (hoistServer appAPI (convertContext ctx) appServer)

-- | Create web application for serving AppAPI
createApp :: AppContext -> Application
createApp ctx = corsMiddleware $ serve serverAPI (server ctx)

-- | Run web application with given DB and on given port
runApplication :: String -> Int -> IO ()
runApplication sqliteFile port = do
    putStrLn $ "Creating SQLite connection pool: " ++ sqliteFile
    dbPool <- runStdoutLoggingT $ createSqlitePool (T.pack sqliteFile) 5
    putStrLn $ "Migrating DB"
    runSqlPool (runMigration migrateAll) dbPool
    putStrLn $ "Migrating DB finished"
    putStrLn $ "---------------------------------------------"
    putStrLn $ "Starting web app on port " ++ show port
    let ctx = AppContext { _appContextPool = dbPool
                         }
    run port (createApp ctx)

