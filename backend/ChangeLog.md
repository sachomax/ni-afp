# Changelog for hw08

## Unreleased changes

- Basic Servant API structure
- Endpoints for Person entity (CRUD)
- Half-done DebtRecord entity endpoints
