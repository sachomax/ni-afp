# NI-AFP
Implement webapp in ELM that will:

1. Allow new user registration and login via social network (facebook, instagram etc) - TBD.
2. Allow to add new people to debt diary without registration - DONE.
3. Create, update, delete entries in debt diary and adding / removing people from the debt diary. - DONE (no entries update, since i considered it to be cheating, only delete or create)
4. Showing total debt status for the logged user. - DONE
5. Add qr code generation that you can send to the person that owes you money. - DONE



First stage - working frontend and backend with bare functionality for 1 user.

Second stage - add new form to add debt between 2 users (People), requires backend upgrade to support (as of 23-06-2022 backend is almost done)

Third stage - add Qr generation between 2 users. (requires small adjustment for backend (adding bank address to person ))

Fourth stage - add google/facebook/etc login and execute create,read for people for every user, and everything else only for logged users (requires a lot of work since i have no idea how to do it)

Fifth stage?? - make frontend look nice? (If there is time left)



## How to run

### Backend 
Navigate to backend folder, run stack build, then stack exec debt-diary. This should launch backend on localhost:3000.
### Frontend
Navigate to frontend folder, run elm react - this will launch frontend on localhost:8000, there you should navigate to Main file. Or you can run elm make src/Main.elm, this will generate html page which you can open.