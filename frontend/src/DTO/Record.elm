module DTO.Record exposing (..)

import Json.Decode as Decode


type alias Record =
    { id : String
    , personIdDebtor : String
    , personIdCreditor : String
    , timestamp : String
    , amount : Float
    }


recordsDecoder : Decode.Decoder (List Record)
recordsDecoder =
    Decode.list recordDecoder


recordDecoder : Decode.Decoder Record
recordDecoder =
    Decode.map5 Record (Decode.field "id" Decode.string) (Decode.field "personIdDebtor" Decode.string) (Decode.field "personIdCreditor" Decode.string) (Decode.field "timestamp" Decode.string) (Decode.field "amount" Decode.float)
