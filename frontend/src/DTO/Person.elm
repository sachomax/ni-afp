module DTO.Person exposing (..)

import Dict
import Dropdown
import Http
import Json.Decode as Decode
import Util.Just exposing (isJust)


type alias Person =
    { id : String
    , firstName : String
    , lastName : String
    , note : String
    , payment : String
    }


personDecoder : Decode.Decoder Person
personDecoder =
    Decode.map5 Person (Decode.field "id" Decode.string) (Decode.field "firstName" Decode.string) (Decode.field "lastName" Decode.string) (Decode.field "note" Decode.string) (Decode.field "payment" Decode.string)


peopleDecoder : Decode.Decoder (List Person)
peopleDecoder =
    Decode.list personDecoder


personFullname : Person -> String
personFullname person =
    person.firstName ++ " " ++ person.lastName


getPersonFullName : Dict.Dict String Person -> String -> String
getPersonFullName dict personId =
    let
        mPerson =
            Dict.get personId dict
    in
    case mPerson of
        Just person ->
            personFullname person

        _ ->
            "Error: Person Not Found"
