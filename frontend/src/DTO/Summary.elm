module DTO.Summary exposing (..)

import Json.Decode as Decode


type alias Summary =
    { total : Float
    , person : String
    }


summaryDecoder : Decode.Decoder Summary
summaryDecoder =
    Decode.map2 Summary (Decode.field "total" Decode.float) (Decode.field "person" Decode.string)
