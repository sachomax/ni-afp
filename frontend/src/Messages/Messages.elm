module Messages.Messages exposing (..)

import DTO.Person exposing (Person)
import DTO.Record exposing (Record)
import DTO.Summary exposing (Summary)
import Dropdown
import Http


type Msg
    = SelectedPerson (Maybe Person)
    | SelectedPersonCreditor (Maybe Person)
    | SelectedPersonDebtor (Maybe Person)
    | SelectedPersonReceiver (Maybe Person)
    | SelectedPersonSender (Maybe Person)
    | DropdownSenderMsg (Dropdown.Msg Person)
    | DropdownReceiverMsg (Dropdown.Msg Person)
    | DropdownPersonMsg (Dropdown.Msg Person)
    | DropdownCreditorMsg (Dropdown.Msg Person)
    | DropdownDebtorMsg (Dropdown.Msg Person)
    | GotTotalAmount (Result Http.Error Summary)
    | GotPeople (Result Http.Error (List Person))
    | ClickedPerson Person
    | GotPersonRecords (Result Http.Error (List Record))
    | AmountChanged String
    | TimestampChanged String
    | DebtNoteChanged String
    | PersonNoteChanged String
    | PersonPaymentChanged String
    | ClickedFormSubmitDebt
    | ClickedFormSubmitPerson
    | LastNameChanged String
    | FirstNameChanged String
    | FailedSaveRecord
    | FailedSavePerson
    | FailedPutPerson
    | SaveRecordResponse (Result Http.Error ())
    | SavePersonResponse (Result Http.Error ())
    | ClickedDeleteRecord String
    | ClickedFormDeletePerson
