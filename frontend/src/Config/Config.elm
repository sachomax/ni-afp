module Config.Config exposing (..)

import DTO.Person exposing (Person)
import Dropdown
import Messages.Messages exposing (Msg(..))


baseApiUrl : String
baseApiUrl =
    "http://localhost:3000/"


senderDropdownConfig : Dropdown.Config Msg Person
senderDropdownConfig =
    Dropdown.newConfig SelectedPersonSender personToDropdownLabel
        |> Dropdown.withItemClass "border-bottom border-silver p1 gray"
        |> Dropdown.withMenuClass "border border-gray"
        |> Dropdown.withMenuStyles [ ( "background", "white" ) ]
        |> Dropdown.withPrompt "Select ..."
        |> Dropdown.withPromptClass "gray"
        |> Dropdown.withSelectedClass "bold"
        |> Dropdown.withSelectedStyles [ ( "color", "black" ) ]
        |> Dropdown.withTriggerClass "col-12 bg-white p1"
        |> Dropdown.withTriggerStyles [ ( "border", "solid #767676" ) ]


receiverDropdownConfig : Dropdown.Config Msg Person
receiverDropdownConfig =
    Dropdown.newConfig SelectedPersonReceiver personToDropdownLabel
        |> Dropdown.withItemClass "border-bottom border-silver p1 gray"
        |> Dropdown.withMenuClass "border border-gray"
        |> Dropdown.withMenuStyles [ ( "background", "white" ) ]
        |> Dropdown.withPrompt "Select ..."
        |> Dropdown.withPromptClass "gray"
        |> Dropdown.withSelectedClass "bold"
        |> Dropdown.withSelectedStyles [ ( "color", "black" ) ]
        |> Dropdown.withTriggerClass "col-12 bg-white p1"
        |> Dropdown.withTriggerStyles [ ( "border", "solid #767676" ) ]


personDropdownConfig : Dropdown.Config Msg Person
personDropdownConfig =
    Dropdown.newConfig SelectedPerson personToDropdownLabel
        |> Dropdown.withItemClass "border-bottom border-silver p1 gray"
        |> Dropdown.withMenuClass "border border-gray"
        |> Dropdown.withMenuStyles [ ( "background", "white" ) ]
        |> Dropdown.withPrompt "Select ..."
        |> Dropdown.withPromptClass "gray"
        |> Dropdown.withSelectedClass "bold"
        |> Dropdown.withSelectedStyles [ ( "color", "black" ) ]
        |> Dropdown.withTriggerClass "col-12 bg-white p1"
        |> Dropdown.withTriggerStyles [ ( "border", "solid #767676" ) ]


personDropdownConfigCreditor : Dropdown.Config Msg Person
personDropdownConfigCreditor =
    Dropdown.newConfig SelectedPersonCreditor personToDropdownLabel
        |> Dropdown.withItemClass "border-bottom border-silver p1 gray"
        |> Dropdown.withMenuClass "border border-gray"
        |> Dropdown.withMenuStyles [ ( "background", "white" ) ]
        |> Dropdown.withPrompt "Select ..."
        |> Dropdown.withPromptClass "gray"
        |> Dropdown.withSelectedClass "bold"
        |> Dropdown.withSelectedStyles [ ( "color", "black" ) ]
        |> Dropdown.withTriggerClass "col-12 bg-white p1"
        |> Dropdown.withTriggerStyles [ ( "border", "solid #767676" ) ]


personDropdownConfigDebtor : Dropdown.Config Msg Person
personDropdownConfigDebtor =
    Dropdown.newConfig SelectedPersonDebtor personToDropdownLabel
        |> Dropdown.withItemClass "border-bottom border-silver p1 gray"
        |> Dropdown.withMenuClass "border border-gray"
        |> Dropdown.withMenuStyles [ ( "background", "white" ) ]
        |> Dropdown.withPrompt "Select ..."
        |> Dropdown.withPromptClass "gray"
        |> Dropdown.withSelectedClass "bold"
        |> Dropdown.withSelectedStyles [ ( "color", "black" ) ]
        |> Dropdown.withTriggerClass "col-12 bg-white p1"
        |> Dropdown.withTriggerStyles [ ( "border", "solid #767676" ) ]


personToDropdownLabel : Person -> String
personToDropdownLabel { firstName, lastName } =
    String.join " " [ firstName, lastName ]
