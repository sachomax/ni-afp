module Main exposing (main)

import Browser
import Config.Config exposing (personDropdownConfig, personDropdownConfigCreditor, personDropdownConfigDebtor, receiverDropdownConfig, senderDropdownConfig)
import DTO.Person exposing (Person, getPersonFullName, personFullname)
import DTO.Record exposing (Record)
import Dict
import Dropdown
import Elements.PersonElements exposing (PersonDetailState(..), PersonDict(..), PersonDropdown, PersonFormData, PersonList(..), QrFormData, deletePerson, getPeople, getSummary, isPersonFormNotEmpty, isPersonFormValid, postPerson, putPerson)
import Elements.RecordElements exposing (FormData, calculateDebt, deleteRecord, getPersonRecords, isFormValid, postRecord)
import Html as Html exposing (img)
import Html.Attributes as Attributes
import Html.Events as Events
import Messages.Messages exposing (Msg(..))
import QRCode
import String exposing (fromFloat, isEmpty, length, toFloat, trim)
import Styles
import Svg.Attributes as SvgA


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }


type alias Model =
    { senderDropdown : PersonDropdown
    , receiverDropdown : PersonDropdown
    , qrFormData : QrFormData
    , personFormData : PersonFormData
    , personDropdown : PersonDropdown
    , personDropdownCreditor : PersonDropdown
    , personDropdownDebtor : PersonDropdown
    , personList : PersonList
    , personDict : PersonDict
    , personDetail : PersonDetailState
    , formData : FormData
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { qrFormData = { sender = Nothing, receiver = Nothing, amount = 0 }
      , senderDropdown =
            { state = Dropdown.newState "personDropdown"
            , selectedPerson = Nothing
            }
      , receiverDropdown =
            { state = Dropdown.newState "personDropdown"
            , selectedPerson = Nothing
            }
      , personFormData = { personId = Nothing, firstName = "", lastName = "", note = "", payment = "" }
      , personDropdown =
            { state = Dropdown.newState "personDropdown"
            , selectedPerson = Nothing
            }
      , personDropdownCreditor =
            { state = Dropdown.newState "personDropdown"
            , selectedPerson = Nothing
            }
      , personDropdownDebtor =
            { state = Dropdown.newState "personDropdown"
            , selectedPerson = Nothing
            }
      , personList = LoadingPersonList
      , personDict = LoadingPersonDict
      , personDetail = NotSelectedPerson
      , formData = { personIdDebtor = Nothing, personIdCreditor = Nothing, timestamp = "", amount = 0, note = "" }
      }
    , getPeople
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ senderDropdown, receiverDropdown, qrFormData, personFormData, personDropdown, personDropdownCreditor, personDropdownDebtor, personList, personDetail, formData } as model) =
    case msg of
        DropdownPersonMsg dropdownMsg ->
            let
                ( updatedDropdownState, dropdownCmd ) =
                    Dropdown.update personDropdownConfig dropdownMsg personDropdown.state

                updatedPersonDropdown =
                    { personDropdown | state = updatedDropdownState }
            in
            ( { model | personDropdown = updatedPersonDropdown }
            , dropdownCmd
            )

        DropdownCreditorMsg dropdownMsg ->
            let
                ( updatedDropdownState, dropdownCmd ) =
                    Dropdown.update personDropdownConfigCreditor dropdownMsg personDropdownCreditor.state

                updatedPersonDropdown =
                    { personDropdownCreditor | state = updatedDropdownState }
            in
            ( { model | personDropdownCreditor = updatedPersonDropdown }
            , dropdownCmd
            )

        DropdownDebtorMsg dropdownMsg ->
            let
                ( updatedDropdownState, dropdownCmd ) =
                    Dropdown.update personDropdownConfigDebtor dropdownMsg personDropdownDebtor.state

                updatedPersonDropdown =
                    { personDropdownDebtor | state = updatedDropdownState }
            in
            ( { model | personDropdownDebtor = updatedPersonDropdown }
            , dropdownCmd
            )

        SelectedPersonCreditor selectedPerson ->
            let
                updatedPersonDropdown =
                    { personDropdownCreditor | selectedPerson = selectedPerson }

                updatedFormData =
                    { formData | personIdCreditor = Maybe.map (\p -> p.id) selectedPerson }
            in
            ( { model | personDropdownCreditor = updatedPersonDropdown, formData = updatedFormData }
            , Cmd.none
            )

        SelectedPersonDebtor selectedPerson ->
            let
                updatedPersonDropdown =
                    { personDropdownDebtor | selectedPerson = selectedPerson }

                updatedFormData =
                    { formData | personIdDebtor = Maybe.map (\p -> p.id) selectedPerson }
            in
            ( { model | personDropdownDebtor = updatedPersonDropdown, formData = updatedFormData }
            , Cmd.none
            )

        SelectedPerson selectedPerson ->
            let
                updatedPersonDropdown =
                    { personDropdown | selectedPerson = selectedPerson }

                updatedFormData =
                    case selectedPerson of
                        Just person ->
                            { personFormData | personId = Maybe.map (\p -> p.id) selectedPerson, lastName = person.lastName, firstName = person.firstName, note = person.note, payment = person.payment }

                        _ ->
                            { personFormData | personId = Maybe.map (\p -> p.id) selectedPerson, lastName = "", firstName = "", note = "", payment = "" }
            in
            ( { model | personDropdown = updatedPersonDropdown, personFormData = updatedFormData }
            , Cmd.none
            )

        GotPeople result ->
            let
                newPeopleDict =
                    case result of
                        Ok people ->
                            SuccessPersonDict <| Dict.fromList (List.map (\p -> ( p.id, p )) people)

                        Err _ ->
                            ErrorPersonDict "Unexpected error appeared when fetching people"

                newPeople =
                    case result of
                        Ok people ->
                            SuccessPersonList <| List.sortBy (\p -> personFullname p) people

                        Err _ ->
                            ErrorPersonList "Unexpected error appeared when fetching people"
            in
            ( { model | personList = newPeople, personDict = newPeopleDict }, Cmd.none )

        ClickedPerson person ->
            ( { model | personDetail = LoadingRecords person }, getPersonRecords person.id )

        GotPersonRecords result ->
            let
                person =
                    case model.personDetail of
                        LoadingRecords modelPerson ->
                            Just modelPerson

                        SuccessRecords modelPerson _ ->
                            Just modelPerson

                        _ ->
                            Nothing

                loadedRecords =
                    case result of
                        Ok records ->
                            case person of
                                Just p ->
                                    SuccessRecords p (List.sortBy (\r -> r.timestamp) records |> List.reverse)

                                Nothing ->
                                    ErrorRecords "Unknown user"

                        Err _ ->
                            ErrorRecords "Unexpected error appeared when fetching person detail"
            in
            ( { model | personDetail = loadedRecords }, Cmd.none )

        FirstNameChanged firstName ->
            let
                updatedFormData =
                    { personFormData | firstName = firstName }
            in
            ( { model | personFormData = updatedFormData }, Cmd.none )

        LastNameChanged lastName ->
            let
                updatedFormData =
                    { personFormData | lastName = lastName }
            in
            ( { model | personFormData = updatedFormData }, Cmd.none )

        AmountChanged amountStr ->
            let
                amount =
                    Maybe.withDefault 0 <| String.toFloat amountStr

                updatedFormData =
                    { formData | amount = amount }
            in
            ( { model | formData = updatedFormData }, Cmd.none )

        TimestampChanged timestamp ->
            let
                updatedFormData =
                    { formData | timestamp = timestamp }
            in
            ( { model | formData = updatedFormData }, Cmd.none )

        DebtNoteChanged note ->
            let
                updatedFormData =
                    { formData | note = note }
            in
            ( { model | formData = updatedFormData }, Cmd.none )

        PersonNoteChanged note ->
            let
                updatedFormData =
                    { personFormData | note = note }
            in
            ( { model | personFormData = updatedFormData }, Cmd.none )

        PersonPaymentChanged payment ->
            let
                updatedFormData =
                    { personFormData | payment = payment }
            in
            ( { model | personFormData = updatedFormData }, Cmd.none )

        ClickedFormSubmitDebt ->
            let
                newState =
                    if isFormValid model.formData then
                        ( { model
                            | formData = FormData Nothing Nothing "" 0 ""
                            , personDropdownCreditor = { state = Dropdown.newState "personDropdown", selectedPerson = Nothing }
                            , personDropdownDebtor = { state = Dropdown.newState "personDropdown", selectedPerson = Nothing }
                          }
                        , postRecord model.formData
                        )

                    else
                        ( model, Cmd.none )
            in
            newState

        ClickedFormSubmitPerson ->
            let
                newState =
                    if isPersonFormValid model.personFormData then
                        ( { model
                            | personFormData = PersonFormData Nothing "" "" "" ""
                            , personDropdown = { state = Dropdown.newState "personDropdown", selectedPerson = Nothing }
                          }
                        , putPerson model.personFormData
                        )

                    else
                        ( { model
                            | personFormData = PersonFormData Nothing "" "" "" ""
                            , personDropdown = { state = Dropdown.newState "personDropdown", selectedPerson = Nothing }
                          }
                        , postPerson model.personFormData
                        )
            in
            newState

        ClickedFormDeletePerson ->
            let
                newState =
                    if isPersonFormValid model.personFormData then
                        ( { model
                            | personFormData = PersonFormData Nothing "" "" "" ""
                            , personDropdown = { state = Dropdown.newState "personDropdown", selectedPerson = Nothing }
                          }
                        , deletePerson (Maybe.withDefault "Nothing" model.personFormData.personId)
                        )

                    else
                        ( model, Cmd.none )
            in
            newState

        FailedSaveRecord ->
            ( model, Cmd.none )

        FailedSavePerson ->
            ( model, Cmd.none )

        FailedPutPerson ->
            ( model, Cmd.none )

        SaveRecordResponse _ ->
            let
                cmd =
                    case model.personDetail of
                        SuccessRecords person _ ->
                            getPersonRecords person.id

                        _ ->
                            Cmd.none
            in
            ( model, cmd )

        SavePersonResponse _ ->
            let
                cmd =
                    getPeople
            in
            ( model, cmd )

        ClickedDeleteRecord recordId ->
            ( model, deleteRecord recordId )

        DropdownSenderMsg dropdownMsg ->
            let
                ( updatedDropdownState, dropdownCmd ) =
                    Dropdown.update senderDropdownConfig dropdownMsg senderDropdown.state

                updatedPersonDropdown =
                    { senderDropdown | state = updatedDropdownState }
            in
            ( { model | senderDropdown = updatedPersonDropdown }
            , dropdownCmd
            )

        DropdownReceiverMsg dropdownMsg ->
            let
                ( updatedDropdownState, dropdownCmd ) =
                    Dropdown.update receiverDropdownConfig dropdownMsg receiverDropdown.state

                updatedPersonDropdown =
                    { receiverDropdown | state = updatedDropdownState }
            in
            ( { model | receiverDropdown = updatedPersonDropdown }
            , dropdownCmd
            )

        SelectedPersonReceiver selectedPerson ->
            let
                updatedPersonDropdown =
                    { receiverDropdown | selectedPerson = selectedPerson }

                updatedFormData =
                    { qrFormData | receiver = Maybe.map (\p -> p.id) selectedPerson, amount = 0 }

                command =
                    case updatedFormData.receiver of
                        Just receiverId ->
                            case qrFormData.sender of
                                Just senderId ->
                                    getSummary senderId receiverId

                                _ ->
                                    Cmd.none

                        _ ->
                            Cmd.none
            in
            ( { model | receiverDropdown = updatedPersonDropdown, qrFormData = updatedFormData }
            , command
            )

        SelectedPersonSender selectedPerson ->
            let
                updatedPersonDropdown =
                    { senderDropdown | selectedPerson = selectedPerson }

                updatedFormData =
                    { qrFormData | sender = Maybe.map (\p -> p.id) selectedPerson, amount = 0 }

                command =
                    case qrFormData.receiver of
                        Just receiverId ->
                            case updatedFormData.sender of
                                Just senderId ->
                                    getSummary senderId receiverId

                                _ ->
                                    Cmd.none

                        _ ->
                            Cmd.none
            in
            ( { model | senderDropdown = updatedPersonDropdown, qrFormData = updatedFormData }
            , command
            )

        GotTotalAmount result ->
            let
                newQrForm =
                    case result of
                        Ok summary ->
                            { qrFormData | amount = summary.total }

                        Err _ ->
                            { qrFormData | amount = -1 }
            in
            ( { model | qrFormData = newQrForm }, Cmd.none )


view : Model -> Html.Html Msg
view model =
    Html.div []
        [ linkBootstrapForDropdown
        , Html.h1 [] [ Html.text "Debt diary" ]
        , viewContent model
        ]



-- Linked bootstrap CSS


linkBootstrapForDropdown : Html.Html msg
linkBootstrapForDropdown =
    Html.node "link"
        [ Attributes.attribute "rel" "stylesheet"
        , Attributes.attribute "href" "https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        , Attributes.attribute "integrity" "sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        , Attributes.attribute "crossorigin" "anonymous"
        ]
        []


viewContent : Model -> Html.Html Msg
viewContent model =
    Html.div Styles.content
        [ personListView model.personList
        , personDetailWrapperView model.personDetail model.personDict
        , addDebtView model
        , addPersonView model
        , addQrView model
        ]


addDebtView : Model -> Html.Html Msg
addDebtView { personDropdownCreditor, personDropdownDebtor, personList, formData } =
    let
        personDropdownOptions =
            case personList of
                SuccessPersonList list ->
                    list

                _ ->
                    []
    in
    Html.div Styles.addDebt
        [ Html.div Styles.dropdown
            [ Html.label Styles.label [ Html.text "Select creditor person" ]
            , Html.map DropdownCreditorMsg (Dropdown.view personDropdownConfigCreditor personDropdownCreditor.state personDropdownOptions personDropdownCreditor.selectedPerson)
            ]
        , Html.div Styles.dropdown
            [ Html.label Styles.label [ Html.text "Select debtor person" ]
            , Html.map DropdownDebtorMsg (Dropdown.view personDropdownConfigDebtor personDropdownDebtor.state personDropdownOptions personDropdownDebtor.selectedPerson)
            ]
        , Html.div []
            [ Html.label Styles.label [ Html.text "Amount" ]
            , Html.input (Styles.input ++ [ Attributes.type_ "number", Events.onInput AmountChanged, Attributes.value (String.fromFloat formData.amount) ]) []
            ]

        -- When sending timestamp do BE, append manually ":00Z" (UTC zone, for this use case it's ok to use hardcoded data for time zone) to data provided by this input
        , Html.div []
            [ Html.label Styles.label [ Html.text "Timestamp" ]
            , Html.input (Styles.input ++ [ Attributes.type_ "datetime-local", Events.onInput TimestampChanged, Attributes.value formData.timestamp ]) []
            ]
        , Html.div []
            [ Html.label Styles.label [ Html.text "Note" ]
            , Html.textarea (Styles.textarea ++ [ Events.onInput DebtNoteChanged, Attributes.value formData.note ]) []
            ]
        , Html.button [ Attributes.disabled (not <| isFormValid formData), Events.onClick ClickedFormSubmitDebt ]
            [ Html.text "Submit" ]
        ]


addPersonView : Model -> Html.Html Msg
addPersonView { personDropdown, personFormData, personDropdownCreditor, personDropdownDebtor, personList, formData } =
    let
        personDropdownOptions =
            case personList of
                SuccessPersonList list ->
                    list

                _ ->
                    []
    in
    Html.div Styles.addPerson
        [ Html.div Styles.dropdown
            [ Html.label Styles.label [ Html.text "Select person to update or don't select to create a new one" ]
            , Html.map DropdownPersonMsg (Dropdown.view personDropdownConfig personDropdown.state personDropdownOptions personDropdown.selectedPerson)
            ]
        , Html.div []
            [ Html.label Styles.label [ Html.text "First Name" ]
            , Html.textarea (Styles.textarea ++ [ Events.onInput FirstNameChanged, Attributes.value personFormData.firstName ]) []
            ]
        , Html.div []
            [ Html.label Styles.label [ Html.text "Last Name" ]
            , Html.textarea (Styles.textarea ++ [ Events.onInput LastNameChanged, Attributes.value personFormData.lastName ]) []
            ]
        , Html.div []
            [ Html.label Styles.label [ Html.text "Note" ]
            , Html.textarea (Styles.textarea ++ [ Events.onInput PersonNoteChanged, Attributes.value personFormData.note ]) []
            ]
        , Html.div []
            [ Html.label Styles.label [ Html.text "Payment" ]
            , Html.textarea (Styles.textarea ++ [ Events.onInput PersonPaymentChanged, Attributes.value personFormData.payment ]) []
            ]
        , Html.button [ Attributes.disabled (not <| isPersonFormNotEmpty personFormData), Events.onClick ClickedFormSubmitPerson ]
            [ Html.text "Submit" ]
        , Html.button [ Attributes.disabled (personDropdown.selectedPerson == Nothing), Events.onClick ClickedFormDeletePerson ]
            [ Html.text "Delete" ]
        ]


addQrView : Model -> Html.Html Msg
addQrView { senderDropdown, receiverDropdown, qrFormData, personDropdown, personFormData, personDropdownCreditor, personDropdownDebtor, personList, formData } =
    let
        personDropdownOptions =
            case personList of
                SuccessPersonList list ->
                    list

                _ ->
                    []
    in
    Html.div Styles.addPerson
        [ Html.div Styles.dropdown
            [ Html.label Styles.label [ Html.text "Select Sender" ]
            , Html.map DropdownSenderMsg (Dropdown.view senderDropdownConfig senderDropdown.state personDropdownOptions senderDropdown.selectedPerson)
            ]
        , Html.div Styles.dropdown
            [ Html.label Styles.label [ Html.text "Select Receiver" ]
            , Html.map DropdownReceiverMsg (Dropdown.view receiverDropdownConfig receiverDropdown.state personDropdownOptions receiverDropdown.selectedPerson)
            ]
        , Html.label Styles.label
            [ Html.text ("Amount: " ++ String.fromFloat qrFormData.amount) ]
        , qrCodeView receiverDropdown
        ]


isReceiverSetUp : QrFormData -> Bool
isReceiverSetUp formData =
    case formData.receiver of
        Just receiver ->
            True

        _ ->
            False


qrCodeView : PersonDropdown -> Html.Html Msg
qrCodeView { state, selectedPerson } =
    case selectedPerson of
        Just person ->
            QRCode.fromString person.payment
                |> Result.map
                    (QRCode.toSvg
                        [ SvgA.width "500px"
                        , SvgA.height "500px"
                        ]
                    )
                |> Result.withDefault (Html.text "Error while encoding to QRCode.")

        _ ->
            Html.text "Error while encoding to QRCode."


personListView : PersonList -> Html.Html Msg
personListView personList =
    Html.div Styles.personList
        [ case personList of
            LoadingPersonList ->
                Html.p [] [ Html.text "Loading..." ]

            ErrorPersonList error ->
                Html.p [] [ Html.text error ]

            SuccessPersonList people ->
                Html.div [] (List.map personView people)
        ]


personView : Person -> Html.Html Msg
personView person =
    Html.div Styles.person
        [ Html.a (Styles.personLink ++ [ Events.onClick <| ClickedPerson person ]) [ Html.text <| personFullname person ]
        ]


personDetailWrapperView : PersonDetailState -> PersonDict -> Html.Html Msg
personDetailWrapperView detailState personDict =
    let
        personDetailContent =
            case detailState of
                NotSelectedPerson ->
                    Html.div [] [ Html.text "No person is selected" ]

                LoadingRecords _ ->
                    Html.div [] [ Html.text "Loading records..." ]

                ErrorRecords error ->
                    Html.div [] [ Html.text error ]

                SuccessRecords person records ->
                    case personDict of
                        SuccessPersonDict people ->
                            personDetailView person records people

                        ErrorPersonDict error ->
                            Html.div [] [ Html.text error ]

                        LoadingPersonDict ->
                            Html.div [] [ Html.text "Loading records..." ]
    in
    Html.div Styles.personDetail
        [ Html.h2 [] [ Html.text "Person detail" ]
        , personDetailContent
        ]


personDetailView : Person -> List Record -> Dict.Dict String Person -> Html.Html Msg
personDetailView person records people =
    case records of
        [] ->
            Html.div []
                [ Html.p []
                    [ personDetailHeaderView person
                    , Html.p [] [ Html.text "No records" ]
                    ]
                ]

        _ ->
            Html.div []
                [ Html.p []
                    [ personDetailHeaderView person ]
                , Html.p [] [ Html.text "Records: " ]
                , Html.ul [] (recordView records people)
                , Html.p [] [ Html.text ("Total debt: " ++ String.fromFloat (calculateDebt records person.id)) ]
                ]


recordView : List Record -> Dict.Dict String Person -> List (Html.Html Msg)
recordView records people =
    case records of
        [] ->
            []

        debtRecord :: rest ->
            Html.li []
                [ Html.div []
                    [ Html.span [] [ Html.text (getPersonFullName people debtRecord.personIdCreditor ++ " Owes: " ++ String.fromFloat debtRecord.amount ++ " To: " ++ getPersonFullName people debtRecord.personIdDebtor ++ " (Time: " ++ debtRecord.timestamp ++ ") ") ]
                    , Html.button [ Events.onClick <| ClickedDeleteRecord debtRecord.id ]
                        [ Html.text "Delete" ]
                    ]
                ]
                :: recordView rest people


personDetailHeaderView : Person -> Html.Html Msg
personDetailHeaderView person =
    Html.div [ Attributes.style "fontWeight" "600" ] [ Html.p [] [ Html.text (personFullname person) ] ]
