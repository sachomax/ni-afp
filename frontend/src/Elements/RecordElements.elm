module Elements.RecordElements exposing (..)

import Config.Config exposing (baseApiUrl)
import DTO.Record exposing (Record, recordsDecoder)
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Messages.Messages exposing (Msg(..))
import Task
import Util.Just exposing (isJust)


type alias FormData =
    { personIdCreditor : Maybe String
    , personIdDebtor : Maybe String
    , timestamp : String
    , amount : Float
    , note : String
    }


type alias ValidFormData =
    { personIdCreditor : String
    , personIdDebtor : String
    , timestamp : String
    , amount : Float
    , note : String
    }


isFormValid : FormData -> Bool
isFormValid { personIdCreditor, personIdDebtor, timestamp, amount, note } =
    if isJust personIdCreditor && isJust personIdDebtor && Maybe.withDefault "NotSelected" personIdCreditor /= Maybe.withDefault "NotSelected" personIdDebtor && amount > 0 && String.length note >= 1 && String.length timestamp >= 1 then
        True

    else
        False


calculateDebt : List Record -> String -> Float
calculateDebt records personId =
    case records of
        [] ->
            0

        record :: rest ->
            if record.personIdDebtor == personId then
                calculateDebt rest personId - record.amount

            else
                calculateDebt rest personId + record.amount


requestEncoder : ValidFormData -> Encode.Value
requestEncoder formData =
    Encode.object [ ( "personIdCreditor", Encode.string formData.personIdCreditor ), ( "personIdDebtor", Encode.string formData.personIdDebtor ), ( "amount", Encode.float formData.amount ), ( "timestamp", Encode.string (formData.timestamp ++ ":00Z") ), ( "note", Encode.string formData.note ) ]


postRecord : FormData -> Cmd Msg
postRecord formData =
    let
        maybeValidFormData =
            if isFormValid formData then
                Just
                    { personIdCreditor = Maybe.withDefault "NotSelected" formData.personIdCreditor
                    , personIdDebtor = Maybe.withDefault "NotSelected" formData.personIdDebtor
                    , amount = formData.amount
                    , timestamp = formData.timestamp
                    , note = formData.note
                    }

            else
                Nothing
    in
    case maybeValidFormData of
        Just validFormData ->
            Http.post
                { url = baseApiUrl ++ "records"
                , body = Http.jsonBody (requestEncoder validFormData)
                , expect = Http.expectWhatever SaveRecordResponse
                }

        Nothing ->
            Task.perform (always FailedSaveRecord) (Task.succeed ())


getPersonRecords : String -> Cmd Msg
getPersonRecords personId =
    Http.get
        { url = baseApiUrl ++ "records?personId=" ++ personId
        , expect = Http.expectJson GotPersonRecords recordsDecoder
        }


deleteRecord : String -> Cmd Msg
deleteRecord recordId =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = baseApiUrl ++ "records/" ++ recordId
        , body = Http.emptyBody
        , expect = Http.expectWhatever SaveRecordResponse
        , timeout = Nothing
        , tracker = Nothing
        }
