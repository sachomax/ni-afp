module Elements.PersonElements exposing (..)

import Config.Config exposing (baseApiUrl)
import DTO.Person exposing (Person, peopleDecoder)
import DTO.Record exposing (Record)
import DTO.Summary exposing (summaryDecoder)
import Dict
import Dropdown
import Http
import Json.Encode as Encode
import Messages.Messages exposing (Msg(..))
import String exposing (length, trim)
import Task
import Util.Just exposing (isJust)


type alias PersonFormData =
    { personId : Maybe String
    , firstName : String
    , lastName : String
    , note : String
    , payment : String
    }


type alias PersonDropdown =
    { state : Dropdown.State
    , selectedPerson : Maybe Person
    }


type PersonDetailState
    = NotSelectedPerson
    | LoadingRecords Person
    | ErrorRecords String
    | SuccessRecords Person (List Record)


type PersonList
    = LoadingPersonList
    | ErrorPersonList String
    | SuccessPersonList (List Person)


type PersonDict
    = LoadingPersonDict
    | ErrorPersonDict String
    | SuccessPersonDict (Dict.Dict String Person)


type alias ValidPersonFormData =
    { personId : String
    , firstName : String
    , lastName : String
    , note : String
    , payment : String
    }


type alias QrFormData =
    { sender : Maybe String
    , receiver : Maybe String
    , amount : Float
    }


type alias ValidQrFormData =
    { sender : String
    , receiver : String
    , amount : Float
    }


isPersonFormValid : PersonFormData -> Bool
isPersonFormValid { personId, firstName, lastName, note, payment } =
    if isJust personId then
        True

    else
        False


isPersonFormNotEmpty : PersonFormData -> Bool
isPersonFormNotEmpty { personId, firstName, lastName, note, payment } =
    if length (trim firstName) > 0 && length (trim lastName) > 0 && length (trim payment) > 0 then
        True

    else
        False


personRequestEncoder : ValidPersonFormData -> Encode.Value
personRequestEncoder formData =
    Encode.object [ ( "personId", Encode.string formData.personId ), ( "firstName", Encode.string formData.firstName ), ( "lastName", Encode.string formData.lastName ), ( "note", Encode.string formData.note ), ( "payment", Encode.string formData.payment ) ]


postPerson : PersonFormData -> Cmd Msg
postPerson personFormData =
    let
        maybeValidFormData =
            Just
                { personId = Maybe.withDefault "NotSelected" personFormData.personId
                , firstName = personFormData.firstName
                , lastName = personFormData.lastName
                , note = personFormData.note
                , payment = personFormData.payment
                }
    in
    case maybeValidFormData of
        Just validFormData ->
            Http.post
                { url = baseApiUrl ++ "people"
                , body = Http.jsonBody (personRequestEncoder validFormData)
                , expect = Http.expectWhatever SavePersonResponse
                }

        Nothing ->
            Task.perform (always FailedPutPerson) (Task.succeed ())


putPerson : PersonFormData -> Cmd Msg
putPerson personFormData =
    let
        maybeValidFormData =
            if isPersonFormValid personFormData then
                Just
                    { personId = Maybe.withDefault "NotSelected" personFormData.personId
                    , firstName = personFormData.firstName
                    , lastName = personFormData.lastName
                    , note = personFormData.note
                    , payment = personFormData.payment
                    }

            else
                Nothing
    in
    case maybeValidFormData of
        Just validFormData ->
            Http.request
                { method = "PUT"
                , headers = []
                , url = baseApiUrl ++ "people/" ++ validFormData.personId
                , body = Http.jsonBody (personRequestEncoder validFormData)
                , expect = Http.expectWhatever SavePersonResponse
                , timeout = Nothing
                , tracker = Nothing
                }

        Nothing ->
            Task.perform (always FailedSavePerson) (Task.succeed ())


deletePerson : String -> Cmd Msg
deletePerson personId =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = baseApiUrl ++ "people/" ++ personId
        , body = Http.emptyBody
        , expect = Http.expectWhatever SavePersonResponse
        , timeout = Nothing
        , tracker = Nothing
        }


getPeople : Cmd Msg
getPeople =
    Http.get
        { url = baseApiUrl ++ "people"
        , expect = Http.expectJson GotPeople peopleDecoder
        }


getSummary : String -> String -> Cmd Msg
getSummary debtorId creditorId =
    Http.get
        { url = baseApiUrl ++ "summary?creditor=" ++ creditorId ++ "&debtor=" ++ debtorId
        , expect = Http.expectJson GotTotalAmount summaryDecoder
        }
